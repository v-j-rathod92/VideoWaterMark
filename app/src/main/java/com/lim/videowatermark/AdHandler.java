package com.lim.videowatermark;

import android.content.Context;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardedVideoAd;

/**
 * Created by Lenovo on 03-02-2018.
 */

public class AdHandler {
    private static AdHandler adHandler;
    private static InterstitialAd mInterstitialReloadAd, mInterstitialAd;
    public static RewardedVideoAd rewardedVideoAd;

    public static AdHandler getInstance(Context context) {
        if (adHandler == null) {
            adHandler = new AdHandler();
            MobileAds.initialize(context, "ca-app-pub-1646721666069669~8222012329");
            rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(context);
            rewardedVideoAd.setRewardedVideoAdListener(null);
        }

        return adHandler;
    }

    public static AdRequest loadAd() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("C21C8AEABCB925D5933727E23B5C2567")
                .build();
        return adRequest;
    }

    public static void loadFullScreenReloadAd(Context context) {
        mInterstitialReloadAd = new InterstitialAd(context);
        mInterstitialReloadAd.setAdUnitId(context.getString(R.string.admob_fullscreen_ad));
        mInterstitialReloadAd.loadAd(loadAd());
        mInterstitialReloadAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();

                mInterstitialReloadAd.loadAd(loadAd());
            }
        });
    }

    public static void loadFullScreenAd(Context context) {
        mInterstitialAd = new InterstitialAd(context);
        mInterstitialAd.setAdUnitId(context.getString(R.string.admob_fullscreen_ad));
        mInterstitialAd.loadAd(loadAd());
    }

    public static void loadVideoAd(Context context) {
        rewardedVideoAd.loadAd(context.getString(R.string.admob_video_ad),
                new AdRequest.Builder().build());
    }

    public static boolean isVideoAdLoaded() {
        return rewardedVideoAd.isLoaded();
    }


    public static void showFullScrenReloadAd() {
        if (mInterstitialReloadAd.isLoaded()) {
            mInterstitialReloadAd.show();
        }
    }

    public static void showFullScrenAd() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    public static void showVideoAd() {
        if (rewardedVideoAd.isLoaded()) {
            rewardedVideoAd.show();
        }
    }

    public static boolean ifAdIsLoaded() {
        return mInterstitialAd.isLoaded();
    }

    public void loadBannerAd(final AdView adView, Context context) {
        AdListener listener = new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                adView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
            }
        };

        adView.setAdListener(listener);
        adView.loadAd(loadAd());
    }
}