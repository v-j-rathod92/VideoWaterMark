package com.lim.videowatermark.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import com.lim.videowatermark.AdHandler;
import com.lim.videowatermark.R;
import com.lim.videowatermark.helper.BitmapCompression;
import com.lim.videowatermark.view.ReselectSpinner;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by rgi-40 on 21/3/18.
 */

public class ConvertToMP3Activity extends BaseActivity implements View.OnClickListener {
    public static final String VIDEO_URI = "video_uri";
    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    private Button btnAddWatermarkSmall, btnAddWatermarkBig;
    private AdView adView;
    private AdHandler adHandler;
    private LinearLayout linearLayoutWatermarkAndSize;
    private AlertDialog dialogItem;
    private File mFileTemp;
    private Uri selectedImageUri;
    private Bitmap selectedImage = null;
    private MediaMetadataRetriever metaRetriever;
    private FrameLayout flEditor;
    private ImageView editor, imgCheck;
    private static final int PICK_IMAGE = 100, WRITE_TEXT = 200;
    private int ht = -1;
    private int wt = -1;
    private String imgpath;
    private ReselectSpinner spinner;
    private TextView txtSize;
    private EditText edtTxtCustomWidth, edtTxtCustomHeight;
    private Button btnOk;
    private String[] spinnerData = {"200 x 200", "300 x 300", "500 x 500", "Custom"};
    private AlertDialog dialogCustomInput;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_convert_to_mp3);

        init();
        setListeners();
        loadAds();
    }

    private void init() {
        mContext = this;

        Toolbar toolbar = findViewById(R.id.toolbar);
        if (getSupportActionBar() != null) {
            setSupportActionBar(toolbar);
        }

        videoView = findViewById(R.id.videoView);
        txtStart = findViewById(R.id.txtStart);
        txtEnd = findViewById(R.id.txtEnd);
        videoSliceSeekBar = findViewById(R.id.videoSliceSeekBar);
        playPauseButton = findViewById(R.id.playPauseButton);
        btnAddWatermarkSmall = findViewById(R.id.btnAddWatermarkSmall);
        btnAddWatermarkBig = findViewById(R.id.btnAddWatermarkBig);
        flEditor = findViewById(R.id.flEditor);
        editor = findViewById(R.id.imgEditImage);
        adView = findViewById(R.id.adView);
        imgCheck = findViewById(R.id.imgCheck);
        txtSize = findViewById(R.id.txtSize);
        linearLayoutWatermarkAndSize = findViewById(R.id.linearLayoutWatermarkAndSize);

        imgCheck.setVisibility(View.VISIBLE);

        imgCheck = toolbar.findViewById(R.id.imgCheck);
        spinner = findViewById(R.id.spinner);

        sourcePath = getIntent().getStringExtra(VIDEO_URI);

        if (sourcePath != null && !sourcePath.equals("")) {
            videoView.setVideoURI(Uri.parse(sourcePath));
        }

        videoView.seekTo(100);

        prepareVideoView();
        loadFFMpegBinary();

        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerData);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spinner.setAdapter(aa);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                Log.e("selected", "ok");
                if (i == 3) {
                    txtSize.setText("Custom");
                    openCustomInputDialog();
                } else {
                    txtSize.setText(spinnerData[i]);

                    String[] size = new String[2];
                    if (i == 0) {
                        size[0] = "100";
                        size[1] = "100";
                    } else if (i == 1) {
                        size[0] = "200";
                        size[1] = "200";
                    } else if (i == 2) {
                        size[0] = "300";
                        size[1] = "300";
                    }

                    changeWatermarkSize(size);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void changeWatermarkSize(String[] size) {
        editor.setImageBitmap(selectedImage);
        FrameLayout.LayoutParams parameter = new FrameLayout.LayoutParams(Integer.parseInt(size[0]), Integer.parseInt(size[1]));
        parameter.leftMargin = ((FrameLayout.LayoutParams) editor.getLayoutParams()).leftMargin;
        parameter.topMargin = ((FrameLayout.LayoutParams) editor.getLayoutParams()).topMargin;
        editor.setLayoutParams(parameter);

        wt = Integer.parseInt(size[0]);
        ht = Integer.parseInt(size[1]);
    }

    private void setListeners() {
        playPauseButton.setOnClickListener(this);
        btnAddWatermarkSmall.setOnClickListener(this);
        btnAddWatermarkBig.setOnClickListener(this);
        imgCheck.setOnClickListener(this);
        txtSize.setOnClickListener(this);
    }

    private void loadAds() {
        adHandler = AdHandler.getInstance(this);
        adHandler.loadBannerAd(adView, this);
        adHandler.showFullScrenReloadAd();
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.seekTo(200);
    }

    private void store() {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/VideoWaterMark/TMPIMG/";
        File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        String filename = "tmp.png";
        File file = new File(dir, filename);
        if (file.exists()) {
            file.delete();
        }
        try {
            if (this.selectedImage != null) {
                FileOutputStream out = new FileOutputStream(file);
                this.selectedImage.compress(Bitmap.CompressFormat.PNG, 100, out);
                out.flush();
                out.close();
                imgpath = path + filename;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.imgCheck:
                if (selectedImage == null) {
                    Toast.makeText(mContext, "Please Add Watermark", Toast.LENGTH_LONG).show();
                } else {
                    MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
                    metaRetriever.setDataSource(sourcePath);

                    String vHeight = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
                    String vWidth = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);

                    float ix = editor.getX();
                    float iy = editor.getY();

                    float videoWidth = Float.parseFloat(vWidth);
                    float videoHeight = Float.parseFloat(vHeight);

                    float videoViewWidth = (float) videoView.getWidth();
                    float videoViewHeight = (float) videoView.getHeight();

                    int xx = (int) ((videoWidth * ix) / videoViewWidth);
                    int yy = (int) ((videoHeight * iy) / videoViewHeight);


                    int scalex = (int) ((videoWidth * ((float) editor.getWidth())) / videoViewWidth);
                    int scaley = ((int) ((videoHeight * ((float) editor.getHeight())) / videoViewHeight));

//                    int scale;
//                    int diff;
//                    if(scalex > scaley){
//                        scale = scalex;
//                        diff = scalex - scaley;
//
//                        yy = (diff * yy) / scaley;
//                    }
//                    else{
//                        scale = scaley;
//                        diff = scaley - scalex;
//
//                        xx = (diff * xx) / scalex;
//                    }


                    selectedImage = Bitmap.createScaledBitmap(selectedImage, scalex, scaley, true);


                    store();
                    cutVideo(imgpath, xx + "", yy + "");
                }
                break;

            case R.id.btnAddWatermarkBig:
            case R.id.btnAddWatermarkSmall:
                showCreateDialog();
                break;

            case R.id.imgImageWatermark:
                mFileTemp = new File(Environment.getExternalStorageDirectory().toString(), TEMP_PHOTO_FILE_NAME);

                Intent photoPickerIntent = new Intent("android.intent.action.PICK");
                photoPickerIntent.setType("image/*");
                photoPickerIntent.putExtra("output", Uri.fromFile(mFileTemp));
                startActivityForResult(photoPickerIntent, PICK_IMAGE);
                if (dialogItem != null)
                    dialogItem.dismiss();
                break;

            case R.id.imgImageEdit:
                dialogItem.dismiss();
                Intent intent = new Intent(mContext, TextEditorActivity.class);
                startActivityForResult(intent, WRITE_TEXT);
                break;

            case R.id.txtSize:
                spinner.performClick();
                break;

            case R.id.btnOk:
                if (validateCustomInput()) {
                    String[] size = new String[2];
                    size[0] = edtTxtCustomWidth.getText().toString();
                    size[1] = edtTxtCustomHeight.getText().toString();
                    changeWatermarkSize(size);

                    if (dialogCustomInput != null) {
                        dialogCustomInput.dismiss();
                    }
                }

                break;
        }
    }

    private boolean validateCustomInput() {
        if (edtTxtCustomWidth.getText().toString().trim().length() == 0) {
            Toast.makeText(this, "Please enter width", Toast.LENGTH_LONG).show();
            return false;
        } else if (edtTxtCustomHeight.getText().toString().trim().length() == 0) {
            Toast.makeText(this, "Please enter height", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void showCreateDialog() {
        View promptView = LayoutInflater.from(this).inflate(R.layout.layout_select_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);

        ImageView imgImageWatermark, imgImageEdit;

        imgImageWatermark = promptView.findViewById(R.id.imgImageWatermark);
        imgImageEdit = promptView.findViewById(R.id.imgImageEdit);

        imgImageWatermark.setOnClickListener(this);
        imgImageEdit.setOnClickListener(this);

        dialogItem = alertDialogBuilder.create();
        dialogItem.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            selectedImageUri = data.getData();

            try {
                String[] filePathColumn = new String[]{"_data"};
                Cursor cursor = getContentResolver().query(this.selectedImageUri, filePathColumn, null, null, null);
                cursor.moveToFirst();
                String picturePath = cursor.getString(cursor.getColumnIndex(filePathColumn[0]));
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 4;
                selectedImage = BitmapFactory.decodeFile(picturePath, options);
                Matrix m = BitmapCompression.adjustImageOrientationUri(this, selectedImageUri);
                selectedImage = Bitmap.createBitmap(selectedImage, 0, 0, selectedImage.getWidth(), selectedImage.getHeight(), m, false);

                ht = videoView.getHeight();
                wt = videoView.getWidth();

                if (selectedImage != null) {
                    metaRetriever = new MediaMetadataRetriever();
                    metaRetriever.setDataSource(sourcePath);
                    wt = Integer.parseInt(metaRetriever.extractMetadata(18));
                    ht = Integer.parseInt(metaRetriever.extractMetadata(19));
                    selectedImage = Bitmap.createScaledBitmap(selectedImage, this.wt, this.ht, true);
                    editor.setImageBitmap(selectedImage);
                    editor.setLayoutParams(new FrameLayout.LayoutParams(200, 200));
                    setImageToCenter(editor);
                    editor.setOnTouchListener(onTouchListener());

                    btnAddWatermarkBig.setVisibility(View.GONE);
                    linearLayoutWatermarkAndSize.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == WRITE_TEXT && resultCode == RESULT_OK) {
            selectedImage = data.getParcelableExtra(TextEditorActivity.BITMAP);
            editor.setImageBitmap(selectedImage);
            editor.setLayoutParams(new FrameLayout.LayoutParams(200, 200));
            setImageToCenter(editor);
            editor.setOnTouchListener(onTouchListener());

            btnAddWatermarkBig.setVisibility(View.GONE);
            linearLayoutWatermarkAndSize.setVisibility(View.VISIBLE);
        }

    }

    private int xDelta;
    private int yDelta;

    private View.OnTouchListener onTouchListener() {
        return (view, event) -> {

            final int x = (int) event.getRawX();
            final int y = (int) event.getRawY();

            switch (event.getAction() & MotionEvent.ACTION_MASK) {

                case MotionEvent.ACTION_DOWN:
                    FrameLayout.LayoutParams lParams = (FrameLayout.LayoutParams)
                            view.getLayoutParams();

                    xDelta = x - lParams.leftMargin;
                    yDelta = y - lParams.topMargin;
                    break;

                case MotionEvent.ACTION_UP:

                    break;

                case MotionEvent.ACTION_MOVE:
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view
                            .getLayoutParams();
                    layoutParams.leftMargin = x - xDelta;
                    layoutParams.topMargin = y - yDelta;
                    layoutParams.rightMargin = 0;
                    layoutParams.bottomMargin = 0;

//                    if (layoutParams.leftMargin < (videoView.getWidth() - selectedImage.getWidth())
//                            && layoutParams.topMargin < (videoView.getHeight() - selectedImage.getHeight())) {
//                        view.setLayoutParams(layoutParams);
//                    }
                    view.setLayoutParams(layoutParams);
                    break;
            }
            return true;
        };
    }

    private void setImageToCenter(ImageView editor) {
        float videoViewWidth = (float) videoView.getWidth();
        float videoViewHeight = (float) videoView.getHeight();

        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) editor
                .getLayoutParams();
        layoutParams.leftMargin = (int) ((videoViewWidth / 2) - 50);
        layoutParams.topMargin = (int) ((videoViewHeight / 2) - 50);
        editor.setLayoutParams(layoutParams);
    }

    private void openCustomInputDialog() {

        View promptView = LayoutInflater.from(this).inflate(R.layout.layout_dialog_custom_size, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);

        edtTxtCustomWidth = promptView.findViewById(R.id.edtTxtCustomWidth);
        edtTxtCustomHeight = promptView.findViewById(R.id.edtTxtCustomHeight);

        btnOk = promptView.findViewById(R.id.btnOk);

        btnOk.setOnClickListener(this);

        dialogCustomInput = alertDialogBuilder.create();
        dialogCustomInput.show();
    }
}
