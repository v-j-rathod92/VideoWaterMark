package com.lim.videowatermark.activity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.ads.AdView;
import com.lim.videowatermark.AdHandler;
import com.lim.videowatermark.R;
import com.lim.videowatermark.permission.GGPermissionManager;
import com.lim.videowatermark.permission.OnRequestPermissionsCallBack;
import com.lim.videowatermark.permission.PermissionRequest;

import hotchemi.android.rate.AppRate;
import hotchemi.android.rate.OnClickButtonListener;

public class MainActivity extends AppCompatActivity {
    private Context mContext;
    private static final int PICK_VIDEO = 101;
    private AdView adView;
    private AdHandler adHandler;
    private boolean isBackPressed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init() {
        mContext = this;

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        adView = findViewById(R.id.adView);

        loadAds();
        launchRateDialog();
    }

    private void loadAds() {
        adHandler = AdHandler.getInstance(this);
        adHandler.loadBannerAd(adView, this);
        adHandler.loadFullScreenReloadAd(this);
        adHandler.loadFullScreenAd(this);
        adHandler.loadVideoAd(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == PICK_VIDEO) {
            String selectedImagePath = data.getStringExtra("selectedVideoPath");

            // MEDIA GALLERY
            try {
                Intent intent = new Intent(mContext, ConvertToMP3Activity.class);
                intent.putExtra(ConvertToMP3Activity.VIDEO_URI, selectedImagePath);
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void pickVideo() {
//        Intent intent = new Intent();
//        intent.setType("video/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select Video"), PICK_VIDEO);

        startActivityForResult(new Intent(mContext, VideoSelectActivity.class), PICK_VIDEO);
    }

    public void selectVideo(View v) {
        checkPermissionAndOpen();
    }


    private void checkPermissionAndOpen() {
        new GGPermissionManager.Builder(mContext)
                .addPermissions(PermissionRequest.readWriteStoragePermission()) //change permission as require
                .addRequestPermissionsCallBack(new OnRequestPermissionsCallBack() {
                    @Override
                    public void onGrant() {
                        pickVideo();
                    }

                    @Override
                    public void onDenied(String permission) {
                        Log.e("permission", "denied");
                    }

                }).build().request();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.share) {

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Video Watermark \n " +
                    "https://play.google.com/store/apps/details?id=" + getPackageName());
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, "Share App"));

        } else if (item.getItemId() == R.id.rate) {

            Uri uri = Uri.parse("market://details?id=" + getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            // To count with Play market backstack, After pressing back button,
            // to taken back to our application, we need to add following flags to intent.
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            try {
                startActivity(goToMarket);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void launchRateDialog() {
        AppRate.with(this)
                .setInstallDays(0) // after 0 days of install, default 10
                .setLaunchTimes(10) // after 3 time launch, default 1
                .setRemindInterval(2) // after 2 days if user select remind me later, default 1
                .setShowLaterButton(true) // default true
                .setDebug(false) // default false
                .setOnClickButtonListener(new OnClickButtonListener() { // callback listener.
                    @Override
                    public void onClickButton(int which) {

                    }
                })
                .monitor();

        // Show a dialog if meets conditions
        AppRate.showRateDialogIfMeetsConditions(this);
    }

    @Override
    public void onBackPressed() {
        if (isBackPressed) {
            super.onBackPressed();
        } else {
            if (adHandler.isVideoAdLoaded()) {
                isBackPressed = true;
                adHandler.showVideoAd();
            } else if (adHandler.ifAdIsLoaded()) {
                isBackPressed = true;
                adHandler.showFullScrenAd();
            } else {
                super.onBackPressed();
            }
        }
    }
}
