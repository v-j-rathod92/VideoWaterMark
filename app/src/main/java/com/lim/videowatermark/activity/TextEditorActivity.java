package com.lim.videowatermark.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lim.videowatermark.R;
import com.lim.videowatermark.utils.ScreenUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import petrov.kristiyan.colorpicker.ColorPicker;

public class TextEditorActivity extends BaseActivity {
    private RelativeLayout relativeLayoutBox;
    private Toolbar toolbar;

    private EditText edtTxtInput;
    private TextView txtText;
    private LinearLayout linearLayoutEnterText;
    private ImageView imgCheck, imgEdit, imgPaint, imgFontsize;
    private Button btnDone;
    private Button btnFont1, btnFont2, btnFont3, btnFont4, btnFont5, btnFont6, btnFont7, btnFont8,
            btnFont9, btnFont10, btnFont11, btnFont12;
    private HorizontalScrollView scrollViewFonts;

    public static final String BITMAP = "bitmap";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_text_editor);

        init();
        setListeners();
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);

        if (getSupportActionBar() != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.text_editor));
        }

        edtTxtInput = findViewById(R.id.edtTxtInput);
        txtText = findViewById(R.id.txtText);
        linearLayoutEnterText = findViewById(R.id.linearLayoutEnterText);
        relativeLayoutBox = findViewById(R.id.relativeLayoutBox);
        imgEdit = findViewById(R.id.imgEdit);
        imgPaint = findViewById(R.id.imgPaint);
        imgFontsize = findViewById(R.id.imgFontsize);
        btnDone = findViewById(R.id.btnDone);

        btnFont1 = findViewById(R.id.btnFont1);
        btnFont2 = findViewById(R.id.btnFont2);
        btnFont3 = findViewById(R.id.btnFont3);
        btnFont4 = findViewById(R.id.btnFont4);
        btnFont5 = findViewById(R.id.btnFont5);
        btnFont6 = findViewById(R.id.btnFont6);
        btnFont7 = findViewById(R.id.btnFont7);
        btnFont8 = findViewById(R.id.btnFont8);
        btnFont9 = findViewById(R.id.btnFont9);
        btnFont10 = findViewById(R.id.btnFont10);
        btnFont11 = findViewById(R.id.btnFont11);
        btnFont12 = findViewById(R.id.btnFont12);

        scrollViewFonts = findViewById(R.id.scrollViewFonts);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) relativeLayoutBox.getLayoutParams();
        params.height = getHeight();

        imgCheck = findViewById(R.id.imgCheck);
        imgCheck.setVisibility(View.VISIBLE);

    }

    public int getHeight() {
        return (int) (ScreenUtils.convertPixelsToDIP(this, ScreenUtils.getScreenWidth(this)) * 0.80);
    }

    private void setListeners() {
        imgCheck.setOnClickListener(this);
        imgEdit.setOnClickListener(this);
        imgPaint.setOnClickListener(this);
        imgFontsize.setOnClickListener(this);
        btnDone.setOnClickListener(this);

        btnFont1.setOnClickListener(this);
        btnFont2.setOnClickListener(this);
        btnFont3.setOnClickListener(this);
        btnFont4.setOnClickListener(this);
        btnFont5.setOnClickListener(this);
        btnFont6.setOnClickListener(this);
        btnFont7.setOnClickListener(this);
        btnFont8.setOnClickListener(this);
        btnFont9.setOnClickListener(this);
        btnFont10.setOnClickListener(this);
        btnFont11.setOnClickListener(this);
        btnFont12.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btnDone:
                String text = edtTxtInput.getText().toString();

                if (text.trim().length() > 0) {
                    txtText.setText(text);
                    linearLayoutEnterText.setVisibility(View.GONE);
                }
                break;

            case R.id.imgEdit:
                linearLayoutEnterText.setVisibility(View.VISIBLE);
                break;

            case R.id.imgPaint:
                final ColorPicker colorPicker = new ColorPicker(this);
                ArrayList<String> colors = new ArrayList<>();
                colors.add("#d50000");
                colors.add("#c51162");
                colors.add("#aa00ff");
                colors.add("#6200ea");
                colors.add("#304ffe");
                colors.add("#2962ff");
                colors.add("#0091ea");
                colors.add("#00b8d4");
                colors.add("#00bfa5");
                colors.add("#00c853");
                colors.add("#64dd17");
                colors.add("#aeea00");
                colors.add("#ffd600");
                colors.add("#ffab00");
                colors.add("#ff6d00");
                colors.add("#dd2c00");
                colors.add("#3e2723");
                colors.add("#212121");
                colors.add("#263238");

                colorPicker
                        .setColors(colors)
                        .setColumns(5)
                        .setRoundColorButton(true)
                        .setOnChooseColorListener(new ColorPicker.OnChooseColorListener() {
                            @Override
                            public void onChooseColor(int position, int color) {
                                txtText.setTextColor(Color.parseColor(colors.get(position)));
                            }

                            @Override
                            public void onCancel() {

                            }
                        });
                colorPicker.show();

                break;

            case R.id.imgFontsize:
                scrollViewFonts.setVisibility(View.VISIBLE);
                break;

            case R.id.btnFont1:
                setFont(ResourcesCompat.getFont(this, R.font.abel));
                break;

            case R.id.btnFont2:
                setFont(ResourcesCompat.getFont(this, R.font.aladin));
                break;

            case R.id.btnFont3:
                setFont(ResourcesCompat.getFont(this, R.font.bevan));
                break;

            case R.id.btnFont4:
                setFont(ResourcesCompat.getFont(this, R.font.carter_one));
                break;

            case R.id.btnFont5:
                setFont(ResourcesCompat.getFont(this, R.font.gravitas_one));
                break;

            case R.id.btnFont6:
                setFont(ResourcesCompat.getFont(this, R.font.lato_medium));
                break;

            case R.id.btnFont7:
                setFont(ResourcesCompat.getFont(this, R.font.opensans_regular));
                break;

            case R.id.btnFont8:
                setFont(ResourcesCompat.getFont(this, R.font.peralta));
                break;

            case R.id.btnFont9:
                setFont(ResourcesCompat.getFont(this, R.font.prosto_one));
                break;

            case R.id.btnFont10:
                setFont(ResourcesCompat.getFont(this, R.font.quicksand_regular));
                break;

            case R.id.btnFont11:
                setFont(ResourcesCompat.getFont(this, R.font.titan_one));
                break;

            case R.id.btnFont12:
                setFont(ResourcesCompat.getFont(this, R.font.vast_shadow));
                break;

            case R.id.imgCheck:
                if (txtText.getText().toString().equals("(Your text)")) {
                    Toast.makeText(this, "Please add your text", Toast.LENGTH_LONG).show();
                } else {
                    screenShot(txtText);
                }

                break;
        }
    }

    private void setFont(Typeface fontFace) {
        txtText.setTypeface(fontFace);
    }

    public void screenShot(View view) {
        Bitmap mbitmap = getBitmapOFRootView(view);

        Intent intent = new Intent();
        intent.putExtra(BITMAP, mbitmap);
        setResult(RESULT_OK, intent);
        finish();
    }

    public Bitmap getBitmapOFRootView(View view) {
        view.setDrawingCacheEnabled(true);
        Bitmap bitmap1 = view.getDrawingCache();
        return bitmap1;
    }
}
