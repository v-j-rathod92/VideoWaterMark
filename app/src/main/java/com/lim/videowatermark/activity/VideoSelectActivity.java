package com.lim.videowatermark.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.lim.videowatermark.R;
import com.lim.videowatermark.adapter.VideoListAdapter;
import com.lim.videowatermark.helper.AutoFitGridLayoutManager;
import com.lim.videowatermark.model.VideoData;
import com.lim.videowatermark.utils.ContentUtill;
import com.lim.videowatermark.utils.ViewHelper;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.ArrayList;
import java.util.List;

public class VideoSelectActivity extends BaseActivity {
    private Context mContext;
    private RecyclerView recyclerView;
    private List<VideoData> videoList = new ArrayList<>();
    private ImageLoader imageLoader;
    private ProgressBar progressBar;
    private VideoListAdapter adapter;
    private LinearLayout layoutEmpty;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_select_video);

        init();
    }

    private void init() {
        mContext = this;

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.title_select_video));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = findViewById(R.id.recyclerView);
        progressBar = findViewById(R.id.progressBar);
        layoutEmpty = findViewById(R.id.layoutEmpty);

        initImageLoader();
        new LoadVideoCursorData().execute();
    }

    private void initImageLoader() {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext()).memoryCache(new WeakMemoryCache()).defaultDisplayImageOptions(new DisplayImageOptions.Builder().cacheInMemory().cacheOnDisc().bitmapConfig(Bitmap.Config.RGB_565).displayer(new FadeInBitmapDisplayer(400)).build()).build();
        this.imageLoader = ImageLoader.getInstance();
        this.imageLoader.init(config);
    }

    private class LoadVideoCursorData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }


        @Override
        protected Void doInBackground(Void... voids) {
            Uri MEDIA_EXTERNAL_CONTENT_URI = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;

            String folderName = getResources().getString(R.string.app_name);
            Cursor cursor = getContentResolver().query(MEDIA_EXTERNAL_CONTENT_URI,
                    new String[]{"_data", "_id", "_display_name", "duration"}, "_data NOT LIKE ? ",
                    new String[]{"%" + folderName + "%"}, " _id DESC");


            int count = cursor.getCount();
            if (count <= 0) {
                return null;
            }

            cursor.moveToFirst();
            for (int i = 0; i < count; i++) {
                Uri uri = Uri.withAppendedPath(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, ContentUtill.getLong(cursor));
                String videoName = cursor.getString(cursor.getColumnIndexOrThrow("_display_name"));
                String videoPath = cursor.getString(cursor.getColumnIndex("_data"));
                long duration = cursor.getLong(cursor.getColumnIndexOrThrow("duration"));

                videoList.add(new VideoData(videoName, uri, videoPath, duration, cursor.getString(cursor.getColumnIndex("_data"))));
                cursor.moveToNext();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (videoList.size() > 0) {
                AutoFitGridLayoutManager layoutManager = new AutoFitGridLayoutManager(mContext, 500);
                recyclerView.setLayoutManager(layoutManager);

                adapter = new VideoListAdapter(mContext, videoList);
                recyclerView.setAdapter(adapter);
                ViewHelper.hideList(recyclerView, layoutEmpty, false);
            } else {
                ViewHelper.hideList(recyclerView, layoutEmpty, true);
            }
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (this.imageLoader != null) {
            this.imageLoader.clearDiskCache();
            this.imageLoader.clearMemoryCache();
        }
    }

    public void onVideoSelected(String videoPath) {
        Intent intent = new Intent();
        intent.putExtra("selectedVideoPath", videoPath);
        setResult(RESULT_OK, intent);
        finish();
    }
}
