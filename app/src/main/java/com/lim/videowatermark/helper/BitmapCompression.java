package com.lim.videowatermark.helper;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class BitmapCompression {
    static int f24h;
    static int f25w;

    public static int calculateInSampleSize(Options options, int reqWidth, int reqHeight) {
        int height = options.outHeight;
        int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            int halfHeight = height / 2;
            int halfWidth = width / 2;
            while (halfHeight / inSampleSize > reqHeight && halfWidth / inSampleSize > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public static Bitmap decodeFile(File f, int reqHeight, int reqWidth) {
        Bitmap bitmap = null;
        try {
            Options o = new Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
            int scale = 1;
            while ((o.outWidth / scale) / 2 >= reqWidth && (o.outHeight / scale) / 2 >= reqHeight) {
                scale *= 2;
            }
            Options o2 = new Options();
            o2.inSampleSize = scale;
            bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return bitmap;
    }

    public static Bitmap adjustImageOrientation(File f, Bitmap image) {
        try {
            int rotate = 0;
            switch (new ExifInterface(f.getAbsolutePath()).getAttributeInt("Orientation", 1)) {
                case 3:
                    rotate = 180;
                    break;
                case 6:
                    rotate = 90;
                    break;
                case 8:
                    rotate = 270;
                    break;
            }
            if (rotate != 0) {
                f25w = image.getWidth();
                f24h = image.getHeight();
                Matrix mtx = new Matrix();
                mtx.preRotate((float) rotate);
                image = Bitmap.createBitmap(image, 0, 0, f25w, f24h, mtx, false);
            }
            return image.copy(Config.ARGB_8888, true);
        } catch (IOException e) {
            return null;
        }
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static Matrix adjustImageOrientationUri(Context context, Uri f) {
        Matrix matrix = new Matrix();
        Cursor cursor = context.getContentResolver().query(f, new String[]{"orientation"}, null, null, null);
        if (cursor.getCount() == 1) {
            cursor.moveToFirst();
            matrix.preRotate((float) cursor.getInt(0));
        }
        return matrix;
    }
}
