package com.lim.videowatermark.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;


public class TouchClass implements OnTouchListener {
    private static Bitmap bm = null;
    private int _xDelta;
    private int _yDelta;
    Bitmap bitmap = null;
    Context context = null;
    private int imghieght = 0;
    private int imgwidth = 0;
    private int whieght = 0;
    private int wwidth = 0;

    public TouchClass(Context c, int wt, int ht) {
        this.wwidth = wt;
        this.whieght = ht;
        this.context = c;
    }

    public boolean onTouch(View v, MotionEvent event) {
        ImageView view = (ImageView) v;
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int X = (int) event.getRawX();
        int Y = (int) event.getRawY();
        this.imgwidth = view.getWidth();
        this.imghieght = view.getHeight();
        switch (event.getAction()) {
            case 0:
                LayoutParams lParams = (LayoutParams) view.getLayoutParams();
                this._xDelta = (int) (((float) X) - view.getTranslationX());
                this._yDelta = (int) (((float) Y) - view.getTranslationY());
                break;
            case 2:
                if (view.getX() >= 0.0f && view.getY() >= 0.0f && view.getX() <= ((float) (this.wwidth - this.imgwidth)) && view.getY() <= ((float) (this.whieght - this.imghieght))) {
                    view.setTranslationX((float) (X - this._xDelta));
                    view.setTranslationY((float) (Y - this._yDelta));
                }
                if (view.getX() > 0.0f || view.getY() > 0.0f) {
                    if (view.getY() > 0.0f || view.getX() < ((float) (this.wwidth - this.imgwidth))) {
                        if (view.getY() < ((float) (this.whieght - this.imghieght)) || view.getX() > 0.0f) {
                            if (view.getX() > 0.0f) {
                                if (view.getY() > 0.0f) {
                                    if (view.getX() < ((float) (this.wwidth - this.imgwidth)) || view.getY() < ((float) (this.whieght - this.imghieght))) {
                                        if (view.getX() < ((float) (this.wwidth - this.imgwidth))) {
                                            if (view.getY() >= ((float) (this.whieght - this.imghieght))) {
                                                view.setTranslationY((float) (this.whieght - this.imghieght));
                                                break;
                                            }
                                        }
                                        view.setTranslationX((float) (this.wwidth - this.imgwidth));
                                        break;
                                    }
                                    view.setTranslationX((float) (this.wwidth - this.imgwidth));
                                    view.setTranslationY((float) (this.whieght - this.imghieght));
                                    break;
                                }
                                view.setTranslationY(0.0f);
                                break;
                            }
                            view.setTranslationX(0.0f);
                            break;
                        }
                        view.setTranslationX(0.0f);
                        view.setTranslationY((float) (this.whieght - this.imghieght));
                        break;
                    }
                    view.setTranslationY(0.0f);
                    view.setTranslationX((float) (this.wwidth - this.imgwidth));
                    break;
                }
                view.setTranslationX(0.0f);
                view.setTranslationY(0.0f);
                break;
        }
        return true;
    }
}
