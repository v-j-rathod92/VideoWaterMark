package com.lim.videowatermark.model;

import android.net.Uri;

public class VideoData {
    public long Duration;
    public Uri VideoUri;
    public long videoId;
    public String videoName;
    public String videoPath;
    public String videoSDPath;

    public VideoData(String videoName, Uri VideoThumbUri, String VideoPath, long Duration, String sd_path) {
        this.videoName = videoName;
        this.VideoUri = VideoThumbUri;
        this.videoPath = VideoPath;
        this.Duration = Duration;
        this.videoSDPath = sd_path;
    }

    public VideoData(String videoName, Uri VideoThumbUri, String VideoPath) {
        this.videoName = videoName;
        this.VideoUri = VideoThumbUri;
        this.videoPath = VideoPath;
    }
}
