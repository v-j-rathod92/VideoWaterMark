package com.lim.videowatermark.utils;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by rgi-40 on 20/3/18.
 */

public class ViewHelper {
    public static void hideList(RecyclerView recyclerView, LinearLayout linearLayoutEmpty, boolean value) {
        if (value) {
            recyclerView.setVisibility(View.GONE);
            linearLayoutEmpty.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            linearLayoutEmpty.setVisibility(View.GONE);
        }
    }
}
