package com.lim.videowatermark.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Spinner;

public class ReselectSpinner extends android.support.v7.widget.AppCompatSpinner {

    OnItemSelectedListener listener;
    int prevPos = -1;
    public ReselectSpinner(Context context) {
        super(context);
    }

    public ReselectSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ReselectSpinner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setSelection(int position) {
        super.setSelection(position);
        if (position == getSelectedItemPosition() && prevPos == position) {
            getOnItemSelectedListener().onItemSelected(null, null, position, 0);
        }
        prevPos = position;
    }
}
